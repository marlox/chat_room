# CHAT ROOM

An application for anyone who would like to make friends and create their own chat room to talk freely about any topic together with several people.
 
 
## Main Features
 
### 1. Chat Room Owner
* Send Message
* Set a Topic
* Remove Guests
 
### 2. Chat Room Guests
* Send Message
* Join Chat Room
 
 
## Specifications
* Ruby Version: 2.4
* Rails Version: 5.0
* Database: MySQL
