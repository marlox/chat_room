class CreateJoinedRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :joined_rooms do |t|
      t.references :room, index: true
      t.references :participant, index: true

      t.boolean :joined, default: true, null: false

      t.timestamps null: false
    end

    add_index :joined_rooms, [:room_id, :participant_id], name: :room_participant_index, unique: true
  end
end
