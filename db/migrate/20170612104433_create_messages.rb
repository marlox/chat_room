class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :content
      t.references :posted_by, index: true
      t.references :joined_room, index: true

      t.timestamps null: false
    end
  end
end
