Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  devise_for :participants, controllers: { sessions: 'participants/sessions' }

  resources :rooms, only: [:new, :create, :show, :index]

  scope :my do
    #
  end

  root to: 'root#index'

end
