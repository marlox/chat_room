require 'test_helper'

class RoomTest < ActiveSupport::TestCase

  test 'must create room' do

    room = create_room

    assert room.present?
    assert room.in?(Room.all)
  end

  test 'must update room' do

    name = 'test update'
    room = create_room

    updated = room.update_attributes! name: name

    assert updated
    assert Room.find(room.id) && room.name == name
  end

  test 'must delete room' do

    room = create_room

    destroyed = room.destroy

    assert destroyed
    assert_not room.in?(Room.all)
  end

  test 'must add participant' do

    room = create_room
    participant = create_participant

    assert room.add(participant.id)
    assert room.added?(participant)
  end

  test 'must remove participant' do

    room = create_room
    participant = create_participant
    room.add(participant.id)

    assert room.remove(participant.id)
    assert room.removed?(participant)
  end

end
