require 'test_helper'

class ParticipantTest < ActiveSupport::TestCase

  test 'must create participant' do

    participant = create_participant

    assert participant.present?
    assert participant.in?(Participant.all)
  end

  test 'must update participant' do

    name = 'testing'
    participant = create_participant

    updated = participant.update_attributes! name: name, password: 'test update', password_confirmation: 'test update'

    assert updated
    assert Participant.find(participant.id) && participant.name == name
  end

  test 'must delete participant' do

    participant = create_participant

    destroyed = participant.destroy

    assert destroyed
    assert_not participant.in?(Participant.all)
  end

  test 'must join a room' do
    room = create_room
    participant = create_participant

    assert participant.join(room.id)
    assert participant.joined?(room)
  end

  test 'must leave a room' do
    room = create_room
    participant = create_participant
    participant.join room.id

    assert participant.leave(room.id)
    assert participant.left?(room)
  end

end
