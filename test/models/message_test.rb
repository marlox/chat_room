require 'test_helper'

class MessageTest < ActiveSupport::TestCase

  def setup
    room = create_room
    @participant = create_participant

    @participant.join(room.id)
    @joined_room = @participant.joined_rooms.first
  end

  test 'must post message by participant' do
    content = 'POST MESSAGE'
    created = @joined_room.messages.create! content: content, participant: @participant

    assert created
    assert @joined_room.messages.find_by content: content, participant: @participant
  end

  test 'must delete message by participant' do
    content = 'DELETE MESSAGE'
    @joined_room.messages.create! content: content, participant: @participant

    message = @joined_room.messages.find_by content: content, participant: @participant

    assert message.destroy
    assert_not message.in?(@joined_room.messages)
  end

  test 'must post message by owner' do
    content = 'POST MESSAGE'
    created = @joined_room.messages.create! content: content, participant: @owner

    assert created
    assert @joined_room.messages.find_by content: content, participant: @owner
  end

  test 'must delete message by owner' do
    content = 'DELETE MESSAGE'
    @joined_room.messages.create! content: content, participant: @owner

    message = @joined_room.messages.find_by content: content, participant: @owner

    assert message.destroy
    assert_not message.in?(@joined_room.messages)
  end

  test 'can owner delete message posted by participant' do
    content = 'OWNER DELETE MESSAGE'
    @joined_room.messages.create! content: content, participant: @participant
    owned_room = @owner.owned_rooms.first
    message = owned_room.messages.find_by content: content, participant: @participant

    assert message.destroy
    assert_not message.in?(owned_room.messages)
  end

end
