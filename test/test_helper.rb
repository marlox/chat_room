ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def starter!
    @email = 'test@test.com'
    @room = 'testing'
    @owner = Participant.new name: 'testing owner', email: "owner.#{@email}", password: "owner.#{@email}", password_confirmation: "owner.#{@email}"
    @owner.save!
  end

  def create_room
    starter!
    room = Room.new name: 'test room', topic: 'test', participant_id: @owner.id
    room.save!
    room
  end

  def create_participant
    starter! if @owner.blank?
    participant = Participant.new name: 'testing man', email: @email, password: @email, password_confirmation: @email
    participant.save!
    participant
  end

end
