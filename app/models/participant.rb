class Participant < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :owned_rooms, class_name: "Room", dependent: :destroy

  has_many :joined_rooms, -> { where(joined: true) }, class_name: "JoinedRoom", foreign_key: :participant_id
  has_many :rooms, through: :joined_rooms, source: :room

  has_many :messages, foreign_key: :posted_by_id

  validates :name, presence: true, uniqueness: true
  validates :email, presence: true, length: { maximum: 255 }, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i }, uniqueness: true

  with_options unless: proc { |p| p.password.blank? } do
    validates :password, presence: true, length: { minimum: 6 }, confirmation: true
    validates :password_confirmation, presence: true, length: { minimum: 6 }
  end

  def join room_id
    room = Room.find_by id: room_id
    unless room.blank?
      joined_room = joined_rooms.find_by room_id: room_id
      if joined_room.blank?
        rooms << room
      else
        joined_room.update_attributes! joined: true
      end
    end
  end

  def leave room_id
    room = joined_rooms.find_by room_id: room_id
    room.update_attributes!(joined: false) unless room.blank?
  end

  def joined? room
    self.in?(room.participants) unless room.blank?
  end

  def left? room
    !joined?(room) unless room.blank?
  end

end
