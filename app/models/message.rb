class Message < ApplicationRecord

  belongs_to :joined_room
  belongs_to :participant, foreign_key: :posted_by_id

  validates :content, presence: true

end
