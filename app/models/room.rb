class Room < ApplicationRecord

  after_save :join_owned_room

  belongs_to :owner, class_name: "Participant", foreign_key: :participant_id

  has_many :joined_participants, -> { where(joined: true) }, class_name: "JoinedRoom", foreign_key: :room_id, dependent: :destroy
  has_many :participants, through: :joined_participants, source: :participant

  has_many :joined_rooms, -> { where(joined: true) }, class_name: "JoinedRoom", foreign_key: :room_id, dependent: :destroy
  has_many :messages, through: :joined_rooms, source: :messages

  def add participant_id
    participant = Participant.find_by id: participant_id
    unless participant.blank?
      joined_participant = joined_participants.find_by participant_id: participant_id
      if joined_participant.blank?
        participants << participant
      else
        joined_participant.update_attributes! joined: true
      end
    end
  end

  def remove participant_id
    participant = joined_participants.find_by participant_id: participant_id
    participant.update_attributes!(joined: false) unless participant.blank?
  end

  def added? participant
    participant.joined?(self) unless participant.blank?
  end

  def removed? participant
    participant.left?(self) unless participant.blank?
  end

private

  def join_owned_room
    add owner.id
  end

end
