class JoinedRoom < ApplicationRecord

  belongs_to :room
  belongs_to :participant

  has_many :messages, dependent: :destroy

end
