module ApplicationCable

  class Connection < ActionCable::Connection::Base

    identified_by :current_participant

    def connect
      self.current_participant = find_verified_participant
      logger.add_tags 'ActionCable', current_participant.email
    end

  protected

    def find_verified_participant # this checks whether a participant is authenticated with devise
      if verified_participant = env['warden'].participant
        verified_participant
      else
        reject_unauthorized_connection
      end
    end

  end

end
