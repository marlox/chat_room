module ApplicationHelper

  def gravatar_for participant, opts = {}
    opts[:alt] = participant.name
    image_tag "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(participant.email)}?s=#{opts.delete(:size) { 40 }}", opts
  end

  def class_for flash_type
    { success: 'success', error: 'danger', warning: 'warning', alert: 'danger',
      notice: 'info', default: 'primary', notice_default: 'muted' }[flash_type.to_sym] || flash_type.to_s
  end

  def icon_for flash_type
    { success: 'check-circle-o', error: 'exclamation-circle', danger: 'ban', alert: 'exclamation-circle',
     notice: 'info-circle', notice_default: 'info-circle' }[flash_type.to_sym] || 'info-circle'
  end

  def flash_messages opts = {}
    render partial: 'layouts/flash_messages'
  end

end
