class RoomsController < ApplicationController

  def index
    @rooms = Room.all

    render action: 'index.html.haml'
  end

  def new
    @room = Room.new

    respond_to{ |f| f.html }
  end

  def create
    @room = current_participant.owned_rooms.build(room_params)
    if @room.save
      flash[:success] = 'Chat room added!'
      redirect_to rooms_path
    else
      render 'new'
    end

    respond_to{ |f| f.html }
  end

  def show
    @room = Room.includes(:messages).find_by(id: params[:id])
    @message = Message.new

    respond_to{ |f| f.html }
  end

private

  def room_params
    params.require(:room).permit(:title)
  end

end
