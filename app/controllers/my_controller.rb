class MyController < ApplicationController

  before_action :owned_rooms

  def index

  end

  def new
    @room = Room.new
  end

  def create
    @room = current_participant.rooms.build(room_params)
    if @room.save
      flash[:success] = 'Chat room added!'
      redirect_to rooms_path
    else
      render 'new'
    end
  end

  def show
    @room = Room.includes(:messages).find_by(id: params[:id])
    @message = Message.new
  end

private

  def owned_rooms
    @rooms = current_participant.owned_rooms
  end

  def room_params
    params.require(:room).permit(:title)
  end

end
