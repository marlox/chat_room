class RootController < ApplicationController

  def index
    path = rooms_path unless current_participant.blank?

    redirect_to path
  end

end
